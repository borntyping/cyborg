//! Test example configuration files

extern crate cyborg_core;

use cyborg_core::Cyborg;
use cyborg_core::config::Load;

#[test]
fn example() {
    let cyborg = Cyborg::load("examples/example.json").unwrap();
    assert_eq!(cyborg.resources.len(), 2);
    assert_eq!(cyborg.rules.len(), 3);
    assert_eq!(cyborg.loggers.len(), 1);
}

#[test]
fn http() {
    Cyborg::load("examples/http.json").unwrap();
}

#[test]
fn mutiny() {
    Cyborg::load("examples/mutiny.json").unwrap();
}
