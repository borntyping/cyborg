//! Test configuration files

extern crate cyborg_core;

use cyborg_core::Cyborg;
use cyborg_core::config::Load;

#[test]
fn test_actions() {
    Cyborg::load("tests/json/test_actions.json").unwrap();
}

#[test]
fn test_conditions() {
    Cyborg::load("tests/json/test_conditions.json").unwrap();
}

#[test]
fn test_resources() {
    Cyborg::load("tests/json/test_resources.json").unwrap();
}

#[test]
fn test_loggers() {
    Cyborg::load("tests/json/test_loggers.json").unwrap();
}

#[test]
#[should_panic]
fn invalid_empty() {
    Cyborg::load("tests/json/invalid_empty.json").unwrap();
}

#[test]
#[should_panic]
fn invalid_not_object() {
    Cyborg::load("tests/json/invalid_not_object.json").unwrap();
}
