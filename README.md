# Cyborg [![](https://img.shields.io/github/tag/borntyping/cyborg.svg)](https://github.com/borntyping/cyborg/tags) [![](https://img.shields.io/travis/borntyping/cyborg.svg)](https://travis-ci.org/borntyping/cyborg) [![](https://img.shields.io/github/issues/borntyping/cyborg.svg)](https://github.com/borntyping/cyborg/issues)

Cyborg is a reactive monitoring system, with the intended capability of being able to react to problems instead of alerting a human user.

* [Source on GitHub](https://github.com/borntyping/cyborg)
* [Packages on Crates.io](https://crates.io/crates/cyborg)
* [Builds on Travis CI](https://travis-ci.org/borntyping/cyborg)

Several components are being built alongside Cyborg:

* [`cyborg-demo`](https://github.com/borntyping/cyborg-demo) demonstrates Cyborg using Mutiny and [Docker](http://docker.io/).
* [`mutiny`](https://github.com/borntyping/mutiny) provides examples of unusual process behaviour.
* [`rust-psutil`](https://github.com/borntyping/rust-psutil) is used to gather process information.
* [`rust-riemann_client`](https://github.com/borntyping/rust-simple_logger) is used to collect information at runtime.
* [`rust-simple_logger`](https://github.com/borntyping/rust-simple_logger) is used for logging.

Usage
-----

Run a very basic configuration with `cargo run -- -N -c example/http.json`. This currently requires [`httpie`](https://github.com/jakubroztocil/httpie) to be installed, and will check if two different sites are available.

Use the [demonstration](https://github.com/borntyping/cyborg-demo) to run a demonstration of Cyborg monitoring and reacting to other processes.

Run `cargo run -- --help` to show information on running Cyborg from the command line. You will need to provide a configuration file. The configuration files in `./examples` and `./tests/json` show various usage examples.

Licence
-------

Cyborg is licenced under the [MIT Licence](http://opensource.org/licenses/MIT).

Authors
-------

Written by [Sam Clements](sam@borntyping.co.uk) as part of a final year university project at the [Aberystwyth University Department of Computer Sciencee](http://www.aber.ac.uk/en/cs/).
