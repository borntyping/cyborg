//! A Condition is passes or fails based on the status of a resource
//!
//! When used as part of a rule, actions are triggered when the condition passes

use std::fmt::Debug;

use time::PreciseTime;

use ::resources::{Process,Service};
use ::rules::{Error,Result};
use ::utils::Ref;

/// Provides the `is_passing` method for Conditions.
pub trait Condition : Debug {
    fn is_passing(&mut self) -> Result<bool>;
}

/// Does nothing, and always passes.
#[derive(Debug)]
pub struct Always;

impl Condition for Always {
    fn is_passing(&mut self) -> Result<bool> {
        Ok(true)
    }
}

/// A list of conditions, passing if all conditions pass.
#[derive(Debug)]
pub struct All {
    pub conditions: Vec<Box<Condition>>
}

impl All {
    pub fn new(conditions: Vec<Box<Condition>>) -> Self {
        All { conditions: conditions }
    }
}

impl Condition for All {
    /// Returns false as soon as a condition does not pass
    fn is_passing<'a>(&'a mut self) -> Result<bool> {
        for condition in self.conditions.iter_mut() {
            if ! try!(condition.is_passing()) {
                return Ok(false);
            }
        }
        return Ok(true);
    }
}

/// A list of conditions, passing if any one of those conditions passes.
#[derive(Debug)]
pub struct Any {
    pub conditions: Vec<Box<Condition>>
}

impl Any {
    pub fn new(conditions: Vec<Box<Condition>>) -> Self {
        Any { conditions: conditions }
    }
}

impl Condition for Any {
    /// Returns true as soon as a condition passes
    fn is_passing(&mut self) -> Result<bool> {
        for condition in self.conditions.iter_mut() {
            if try!(condition.is_passing()) {
                return Ok(true);
            }
        }
        return Ok(false);
    }
}

/// Uses Command as a condition - passes if the script exits successfully.
#[derive(Debug)]
pub struct Command {
    /// Arguments passed to `::utils::run_command`
    args: Vec<String>,

    /// True if the it matches on success, false if it matches on failure.
    expect_success: bool
}

impl Command {
    pub fn new(args: Vec<String>, expect_success: bool) -> Self {
        Command { args: args, expect_success: expect_success }
    }
}

impl Condition for Command {
    fn is_passing(&mut self) -> Result<bool> {
        let output = try!(::utils::run_command(&self.args));

        Ok(self.expect_success == output.status.success())
    }
}

/// Checks if a process is using more than N% CPU.
pub struct CPULimit {
    resource: Ref<Process>,
    /// The time and process snapshot from the last check.
    past: Option<(PreciseTime, ::psutil::process::Process)>,
    /// When the CPU usage is above this limit, the condition will pass.
    limit: f64
}

impl CPULimit {
    pub fn new(resource: Ref<Process>, limit: f64) -> Self {
        CPULimit {
            resource: resource,
            past: None,
            limit: limit
        }
    }
}

impl Condition for CPULimit {
    fn is_passing(&mut self) -> Result<bool> {
        let current_time = PreciseTime::now();
        let current_proc = self.resource.process();

        let result = if let Some((past_time, ref past_proc)) = self.past {
            // Skip the check if the process has changed.
            if current_proc.pid != past_proc.pid
            || current_proc.starttime != past_proc.starttime {
                return Err(Error::from("Process has changed"));
            }

            // Duration is measured in seconds, but `num_milliseconds` is
            // used becuase `Duration` rounds results to integer values.
            let duration = {
                past_time.to(current_time).num_milliseconds() as f64 / 1000.0
            };

            // If no (significant) duration has passed, it's not possible to
            // work out an average value as the average will be `NaN`.
            if duration == 0.0 {
                return Err(Error::from("No time has passed since last check"));
            }

            // Calculate average CPU usage since the last check.
            let average_cpu_usage: f64 = {
                let current_cpu_usage = current_proc.stime + current_proc.utime;
                let past_cpu_usage = past_proc.stime + past_proc.utime;
                (current_cpu_usage - past_cpu_usage) / duration * 100.0
            };

            // Log information about the condition's state.
            info!(
                "PID {} has an average CPU usage of {:.2}% \
                over the past {:.2} seconds",
                current_proc.pid, average_cpu_usage, duration);

            if average_cpu_usage > self.limit {
                warn!("PID {} is using more than {}% CPU ({}%)",
                    current_proc.pid, self.limit, average_cpu_usage);
            } else {
                info!("PID {} is using less than {}% CPU ({}%)",
                    current_proc.pid, self.limit, average_cpu_usage);
            }

            Ok(average_cpu_usage > self.limit)
        } else {
            Err(Error::from("No previous process snapshot availible, \
                cannot compare CPU usage"))
        };

        // Regardless of the result, `.past` should be updated for use in the
        // next check.
        self.past = Some((current_time, current_proc));

        return result;
    }
}

impl ::std::fmt::Debug for CPULimit {
    fn fmt(&self, f: &mut ::std::fmt::Formatter) -> ::std::fmt::Result {
        write!(f, "CPULimit {{ resource: {:?}, limit: {} }}", self.resource, self.limit)
    }
}

/// Does nothing and never passes.
#[derive(Debug)]
pub struct Never;

impl Condition for Never {
    fn is_passing(&mut self) -> Result<bool> {
        Ok(false)
    }
}

/// Triggers when a process uses more than N bytes of memory.
#[derive(Debug)]
pub struct MemoryLimit {
    pub resource: Ref<Process>,
    pub limit: u64
}

impl MemoryLimit {
    pub fn new(resource: Ref<Process>, limit: u64) -> Self {
        MemoryLimit { resource: resource, limit: limit }
    }
}

impl Condition for MemoryLimit {
    fn is_passing(&mut self) -> Result<bool> {
        let process = self.resource.process();
        let memory = try!(process.memory());
        let limit_break = memory.resident > self.limit;

        info!("PID {} is using {}b of {}b ({})",
            process.pid, memory.resident, self.limit,
            if limit_break { "failed" } else { "passed" });

        Ok(limit_break)
    }
}

/// Checks if a service is running
#[derive(Debug)]
pub struct Status {
    pub resource: Ref<Service>
}

impl Condition for Status {
    fn is_passing(&mut self) -> Result<bool> {
        Ok(try!(self.resource.status()))
    }
}

#[cfg(test)]
mod test {
    extern crate simple_logger;

    use std::rc::Rc;

    use ::rules::conditions::Condition;
    use ::rules::conditions::{Always,Never};

    #[test]
    fn always() {
        assert!(Always.is_passing().unwrap() == true);
    }

    #[test]
    fn never() {
        assert!(Never.is_passing().unwrap() == false);
    }

    #[test]
    fn cpulimit() {
        use ::resources::Cyborg;
        use ::rules::conditions::CPULimit;

        let mut condition = CPULimit::new(Rc::new(Box::new(Cyborg)), 0.0);

        assert!(condition.is_passing().is_err(),
            "First CPULimit check should fail as no process has been stored");

        for _ in 0..2 {
            // Do some 'work' so the CPU notices us
            for i in 0..4096 {
                for j in 0..4096 {
                    i * j;
                }
            }

            assert!(condition.is_passing().unwrap() == true,
                "The check should always pass when a CPU limit is set at 0%");
        }
    }

    #[test]
    fn memorylimit() {
        use ::resources::Cyborg;
        use ::rules::conditions::MemoryLimit;

        let mut condition = MemoryLimit::new(Rc::new(Box::new(Cyborg)), 0);
        assert!(condition.is_passing().unwrap() == true);
    }

    mod all {
        use ::rules::conditions::{All,Always,Condition,Never};

        #[test]
        fn always() {
            assert!(All {
                conditions: vec![Box::new(Always), Box::new(Always)]
            }.is_passing().unwrap() == true);
        }

        #[test]
        fn mixed() {
            assert!(All {
                conditions: vec![Box::new(Always), Box::new(Never)]
            }.is_passing().unwrap() == false);
        }

        #[test]
        fn never() {
            assert!(All {
                conditions: vec![Box::new(Never), Box::new(Never)]
            }.is_passing().unwrap() == false);
        }
    }

    mod any {
        use ::rules::conditions::{Any,Always,Condition,Never};

        #[test]
        fn always() {
            assert!(Any {
                conditions: vec![Box::new(Always), Box::new(Always)]
            }.is_passing().unwrap() == true);
        }

        #[test]
        fn mixed() {
            assert!(Any {
                conditions: vec![Box::new(Always), Box::new(Never)]
            }.is_passing().unwrap() == true);
        }

        #[test]
        fn never() {
            assert!(Any {
                conditions: vec![Box::new(Never), Box::new(Never)]
            }.is_passing().unwrap() == false);
        }
    }

    mod command {
        use ::rules::conditions::{Condition,Command};

        #[test]
        fn exit_success() {
            assert!(Command::new(vec!["true".to_string()], true).is_passing().unwrap() == true);
            assert!(Command::new(vec!["true".to_string()], false).is_passing().unwrap() == false);
        }

        #[test]
        fn exit_failure() {
            assert!(Command::new(vec!["false".to_string()], true).is_passing().unwrap() == false);
            assert!(Command::new(vec!["false".to_string()], false).is_passing().unwrap() == true);
        }
    }
}
