//! Basic data structures used to represent rules

pub mod conditions;
pub mod actions;
mod error;

pub use self::error::Error;

/// A result that can be collected from an `Action` or `Condition`
pub type Result<T> = ::std::result::Result<T, Error>;

/// A rule owns a condition and action
///
/// Both the condition and action boxes could use a lifetime specific to `Rule`,
/// but means specifiying lifetimes throughout the rest of the codebase. In
/// pratices, the `Rule` will exist until the program exists anyway.
#[derive(Debug)]
pub struct Rule {
    pub name: String,
    pub condition: Box<conditions::Condition>,
    pub action: Box<actions::Action>
}

impl Rule {
    pub fn tick(&mut self) -> Result<bool> {
        info!("Checking condition for rule '{}'", self.name);

        let action_triggered = match self.condition.is_passing() {
            Ok(x) => x,
            Err(e) => {
                error!("Condition for rule '{}' raised an error: {:?}", self.name, e);
                return Err(e);
            }
        };

        if action_triggered {
            info!("Condition matched, performing action for rule '{}'", self.name);
            if let Err(e) = self.action.tick() {
                error!("Action for rule '{}' raised an error: {:?}", self.name, e);
                return Err(Error::from(e));
            }
            return Ok(true);
        } else {
            info!("Condition did not match for rule '{}'", self.name);
            return Ok(false);
        }
    }
}
