//! Actions that do something with a Resource when a Condition is triggered.

use std::fmt::Debug;
use std::io::{Error,ErrorKind,Result};

use ::resources::Process;
use ::resources::Service;
use ::utils::Ref;

/// Provides the `tick` method for actions
pub trait Action: Debug {
    fn tick(&self) -> Result<()>;
}


/// Runs a command
#[derive(Debug)]
pub struct Command {
    args: Vec<String>,
}

impl Command {
    pub fn new(args: Vec<String>) -> Self {
        Command { args: args }
    }
}

impl Action for Command {
    fn tick(&self) -> Result<()> {
        if try!(::utils::run_command(&self.args)).status.success() {
            Ok(())
        } else {
            Err(Error::new(ErrorKind::Other, "Failed to run command"))
        }
    }
}

/// Does nothing.
#[derive(Debug)]
pub struct Nothing;

impl Action for Nothing {
    fn tick(&self) -> Result<()> {
        trace!("Doing nothing");
        return Ok(());
    }
}

#[derive(Debug)]
pub struct Restart {
    pub resource: Ref<Service>
}

impl Action for Restart {
    fn tick(&self) -> Result<()> {
        info!("Restarting service {:?}", self.resource);
        self.resource.restart()
    }
}

/// Sends SIGTERM to a proccess.
#[derive(Debug)]
pub struct Signal {
    pub resource: Ref<Process>
}

impl Action for Signal {
    fn tick(&self) -> Result<()> {
        let process = self.resource.process();
        process.kill().unwrap();
        warn!("Sent SIGTERM to PID {}", process.pid);
        return Ok(());
    }
}


#[derive(Debug)]
pub struct Start {
    pub resource: Ref<Service>
}

impl Action for Start {
    fn tick(&self) -> Result<()> {
        info!("Starting service {:?}", self.resource);
        self.resource.start()
    }
}

#[derive(Debug)]
pub struct Stop {
    pub resource: Ref<Service>
}

impl Action for Stop {
    fn tick(&self) -> Result<()> {
        info!("Stopping service {:?}", self.resource);
        self.resource.stop()
    }
}
