/// Error enum for the `rules` module

/// Error type for condtions and actions
#[derive(Debug)]
pub enum Error {
    Io(::std::io::Error),
    Misc(String)
}

impl ::std::fmt::Display for Error {
    fn fmt(&self, f: &mut ::std::fmt::Formatter) -> ::std::fmt::Result {
        use std::error::Error;

        write!(f, "{}", self.description())
    }
}

impl ::std::error::Error for Error {
    fn description(&self) -> &str {
        match self {
            &Error::Io(ref e) => e.description(),
            &Error::Misc(ref s) => s
        }
    }

    fn cause<'a>(&'a self) -> Option<&'a ::std::error::Error> {
        match self {
            &Error::Io(ref e) => Some(e),
            &Error::Misc(_) => None
        }
    }
}

impl From<::std::io::Error> for Error {
    fn from(err: ::std::io::Error) -> Self {
        Error::Io(err)
    }
}

impl From<&'static str> for Error {
    fn from(err: &'static str) -> Self {
        Error::Misc(err.to_string())
    }
}
