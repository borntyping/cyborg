//! Resources used by conditions and actions.

use std::fmt::Debug;
use std::io::{Error,ErrorKind,Result};
use std::path::PathBuf;
use std::rc::Rc;
use std::str::from_utf8;

use psutil;

use ::utils::Ref;
use ::utils::run_command;

#[cfg(test)]
mod test;

macro_rules! into_resource_impl {
    ($resource_struct:path, $resource_type:path) => (
        impl Into<Resource> for $resource_struct {
            fn into(self) -> Resource {
                $resource_type(Rc::new(Box::new(self)))
            }
        }
    )
}

/// An enum of possible resources by the trait they implement
pub enum Resource {
    Process(Ref<Process>),
    Service(Ref<Service>)
}

/// A trait for resources that represent a specific process
pub trait Process : Debug {
    /// Returns a snapshot of the process using `psutil`
    fn process(&self) -> psutil::process::Process;
}

/// A trait for resources that represent a service
pub trait Service : Debug {
    /// Attempts to start the service.
    fn start(&self) -> Result<()>;

    /// Attempts to stop the service.
    fn stop(&self) -> Result<()>;

    /// Returns true if the service is running.
    fn status(&self) -> Result<bool>;

    /// Attempts to restart the service.
    fn restart(&self) -> Result<()>;
}

/// A resource that returns a `Process` describing the current Cyborg instance.
#[derive(Debug)]
pub struct Cyborg;

into_resource_impl!(Cyborg, Resource::Process);

impl Process for Cyborg {
    fn process(&self) -> psutil::process::Process {
        info!("Current process is assigned PID {}", psutil::getpid());
        psutil::process::Process::new(psutil::getpid()).unwrap()
    }
}

/// A resource that loads a `Process` from a pidfile.
#[derive(Debug)]
pub struct Pidfile {
    path: PathBuf
}

into_resource_impl!(Pidfile, Resource::Process);

impl Pidfile {
    pub fn new<T: Into<PathBuf> + ?Sized>(path: T) -> Self {
        Pidfile { path: path.into() }
    }
}

impl Process for Pidfile {
    fn process(&self) -> psutil::process::Process {
        let pid = psutil::pidfile::read_pidfile(&self.path).unwrap();
        info!("Read PID {} from {}", pid, self.path.display());
        return psutil::process::Process::new(pid).unwrap();
    }
}

/// Manages a System V init script using `service(8)`
#[derive(Debug)]
pub struct SysV {
    pub name: String
}

into_resource_impl!(SysV, Resource::Service);

impl SysV {
    fn service_command(&self, action: &str) -> Result<::std::process::Output> {
        run_command(&vec![
            "service".to_string(),
            self.name.clone(),
            action.to_string()
        ])
    }

    fn service_command_ok(&self, action: &str) -> Result<()> {
        self.service_command(action).and_then(|_| Ok(()))
    }
}

impl Service for SysV {
    fn start(&self) -> Result<()> { self.service_command_ok("start") }
    fn stop(&self) -> Result<()> { self.service_command_ok("stop") }

    fn status(&self) -> Result<bool> {
        self.service_command("status").and_then(|o| Ok(o.status.success()))
    }

    fn restart(&self) -> Result<()> { self.service_command_ok("restart") }
}

/// Manages a supervisord service using `supervisorctl`
#[derive(Debug)]
pub struct Supervisorctl {
    pub name: String
}

into_resource_impl!(Supervisorctl, Resource::Service);

impl Supervisorctl {
    fn command(&self, action: &str) -> Result<::std::process::Output> {
        let output = try!(run_command(&vec![
            "supervisorctl".to_string(),
            action.to_string(),
            self.name.clone()
        ]));

        if !output.status.success() {
            Err(Error::new(ErrorKind::Other,
                "supervisorctl exited with a non-zero exit code"))
        } else {
            Ok(output)
        }
    }

    fn command_ok(&self, action: &str) -> Result<()> {
        self.command(action).and_then(|_| Ok(()))
    }
}

impl Service for Supervisorctl {
    fn start(&self) -> Result<()> { self.command_ok("start") }
    fn stop(&self) -> Result<()> { self.command_ok("stop") }
    fn restart(&self) -> Result<()> { self.command_ok("restart") }

    fn status(&self) -> Result<bool> {
        match from_utf8(&try!(self.command("status")).stdout) {
            Ok(stdout) => Ok(stdout.matches("RUNNING").count() > 0),
            Err(e) => Err(Error::new(ErrorKind::Other, e))
        }
    }
}

