use psutil::getpid;
use psutil::pidfile::write_pidfile;
use tempdir::TempDir;

use super::{Process,Cyborg,Pidfile};

#[test]
fn test_cyborg_resource() {
    let cyborg: Box<Process> = Box::new(Cyborg);
    assert_eq!(cyborg.process().pid, getpid());
}

#[test]
fn test_pidfile_resource() {
    let tempdir = TempDir::new("cyborg-pidfile-test").unwrap();
    let pidfile = tempdir.path().join("cyborg.pid");
    write_pidfile(&pidfile).unwrap();

    let resource: Box<Process> = Box::new(Pidfile { path: pidfile });
    assert_eq!(resource.process().pid, getpid());
}
