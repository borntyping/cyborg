//! Utilities used by other modules.

use std::io::{BufRead,Cursor,Result};
use std::process;
use std::rc::Rc;
use std::slice::SliceConcatExt;

/// A reference type used to point to shared resources by trait.
///
/// The `Rc` type does not currently accept traits, event though it's underlying
/// implementation uses `Box`. For more information, see [this
/// issue](https://github.com/rust-lang/rust/issues/18248).
pub type Ref<T> = Rc<Box<T>>;

/// Runs a command defined by a list of strings
///
/// Logs STDOUT, STDERR and the exit code if the program did not exit
/// sucessfully.
pub fn run_command(args: &Vec<String>) -> Result<process::Output> {
    let mut command = process::Command::new(&args[0]);
    command.args(&args[1..]);

    trace!("Running command '{}'", args.connect(" "));
    let output = try!(command.output());

    if !output.status.success() {
        let output_trace = output.clone();

        for line in Cursor::new(output_trace.stdout).lines() {
            trace!("STDOUT: {}", try!(line));
        }

        for line in Cursor::new(output_trace.stderr).lines() {
            trace!("STDERR: {}", try!(line));
        }

        if let Some(code) = output_trace.status.code() {
            trace!("STATUS: Command '{}' exited with code {}",
                args.connect(" "), code);
        }
    }

    return Ok(output);
}
