//! These are the internals used by the [cyborg](../cyborg/index.html)
//! executable.

#![feature(collections,plugin,std_misc)]
#![cfg_attr(test, feature(test))]
#![plugin(docopt_macros)]

extern crate docopt;
#[macro_use] extern crate log;
extern crate psutil;
extern crate time;
#[cfg(test)] extern crate tempdir;
#[cfg(test)] extern crate test;
extern crate riemann_client;
extern crate rustc_serialize;

use std::collections::HashMap;
use std::time::duration::Duration;
use std::thread::sleep_ms;

use ::loggers::Logger;
use ::resources::Resource;
use ::rules::Rule;

pub mod config;
pub mod loggers;
pub mod resources;
pub mod rules;
pub mod utils;

/// A Cyborg instance contains resources and rules.
///
/// TODO: Include `start` and `once` functions for single Cyborg instances
pub struct Cyborg {
    pub loggers: Vec<Box<Logger>>,
    pub resources: HashMap<String, Resource>,
    pub rules: Vec<Rule>
}

impl Cyborg {
    /// Creates a blank Cyborg instance.
    pub fn new() -> Self {
        Cyborg {
            loggers: Vec::new(),
            resources: HashMap::new(),
            rules: Vec::new()
        }
    }

    /// Calls `tick` every 2 seconds
    pub fn start(&mut self) {
        self.init();
        loop { interval(Duration::seconds(2), Duration::span(|| self.tick())) }
    }

    /// Runs `init` and then calls `tick` once
    pub fn once(&mut self) {
        self.init();
        self.tick();
    }

    /// Runs any functions that should be run once before starting to tick.
    fn init(&mut self) {
        for logger in self.loggers.iter_mut() {
            logger.init();
        }
    }

    /// Runs a single tick, checking each rule in turn.
    fn tick(&mut self) {
        for rule in self.rules.iter_mut() {
            let rule_result = rule.tick();

            for logger in self.loggers.iter_mut() {
                logger.log_rule(&rule, &rule_result);
            }
        }
    }
}

/// Run a function with at least the specified duration between calls.
///
/// If the function takes longer than that, the next call will be immediate.
fn interval(min_time: Duration, time_taken: Duration) {
    let remaining_time = min_time - time_taken;
    if remaining_time > Duration::zero() {
        // This is really silly, as sleep_ms creates a Duration struct
        // internally and casts `remaining_time` back to i64.
        sleep_ms(remaining_time.num_milliseconds() as u32);
    }
}

/// A group of `Cyborg` instances that can be run at the same time.
pub struct CyborgArmy {
    cyborgs: Vec<Cyborg>
}

impl CyborgArmy {
    /// Calls `Cyborg.tick()` on each `Cyborg` instance every 2 seconds
    pub fn start(&mut self) {
        self.init();

        loop {
            interval(Duration::seconds(2), Duration::span(|| self.tick()));
        }
    }

    // Runs `init` and then `tick` once
    pub fn once(&mut self) {
        self.init();
        self.tick();
    }

    /// Runs startup functions for each `Cyborg` instance.
    fn init(&mut self) {
        for cyborg in self.cyborgs.iter_mut() {
            cyborg.init();
        }
    }

    /// Runs a single tick for each `Cyborg` instance
    fn tick(&mut self) {
        for cyborg in self.cyborgs.iter_mut() {
            cyborg.tick()
        }
    }
}
