//! A wrapper around the `rustc_serialize` crate, providing extensions to the
//! `json` module that return `Result` instead of `Option`.

pub mod json {
    pub use rustc_serialize::json::{Array,DecoderError,Json,Object,ParserError};

    pub trait JsonExt {
        /// Return the `Json` as an `Object` or error
        fn into_object(&self) -> Result<&Object, DecoderError>;

        /// Extends `as_string` to return a `String` instead of `&str`
        fn as_owned_string(&self) -> Option<String>;
    }

    impl JsonExt for Json {
        fn into_object(&self) -> Result<&Object, DecoderError> {
            self.as_object().ok_or(DecoderError::ApplicationError(
                format!("Expected object, got {:?}", self)))
        }

        fn as_owned_string(&self) -> Option<String> {
            match *self {
                Json::String(ref s) => Some(s.clone()),
                _ => None
            }
        }
    }

    /// Shortcut macro for an ApplicationError that expected an object property
    /// to have a certain type
    macro_rules! expected {
        ($name:expr, $ty:ty) => {
            DecoderError::ApplicationError(
                format!("Expected to read {} for property '{}'",
                    stringify!($ty), $name))
        }
    }

    pub trait ObjectExt {
        fn get_property(&self, name: &str) -> Result<&Json, DecoderError>;

        fn get_array(&self, name: &str) -> Result<&Array, DecoderError>;
        fn get_string_array(&self, name: &str) -> Result<Vec<String>, DecoderError>;
        fn get_object_array(&self, name: &str) -> Result<Vec<&Object>, DecoderError>;

        fn get_object(&self, name: &str) -> Result<&Object, DecoderError>;
        fn get_boolean(&self, name: &str) -> Result<bool, DecoderError>;
        fn get_f64(&self, name: &str) -> Result<f64, DecoderError>;
        fn get_u64(&self, name: &str) -> Result<u64, DecoderError>;
        fn get_str(&self, name: &str) -> Result<&str, DecoderError>;
        fn get_string(&self, name: &str) -> Result<String, DecoderError>;
    }

    /// Extends rustc_serialize::json::Object with methods that return Result
    impl ObjectExt for Object {
        fn get_property(&self, name: &str) -> Result<&Json, DecoderError> {
            self.get(name).ok_or(DecoderError::ApplicationError(
                format!("Expected object {:?} to have property '{}'", self, name)))
        }

        fn get_array(&self, name: &str) -> Result<&Array, DecoderError> {
            try!(self.get_property(name)).as_array().ok_or(expected!(name, Array))
        }

        fn get_object_array(&self, name: &str) -> Result<Vec<&Object>, DecoderError> {
            try!(self.get_array(name)).iter().map(|j| { j.into_object() }).collect()
        }

        fn get_string_array(&self, name: &str) -> Result<Vec<String>, DecoderError> {
            try!(self.get_array(name)).iter().map(|json| {
                json.as_owned_string().ok_or(DecoderError::ApplicationError(
                    format!("Expected property {} to only contain strings", name)))
            }).collect()
        }

        fn get_object(&self, name: &str) -> Result<&Object, DecoderError> {
            try!(self.get_property(name)).as_object().ok_or(expected!(name, Object))
        }

        fn get_boolean(&self, name: &str) -> Result<bool, DecoderError> {
            try!(self.get_property(name)).as_boolean().ok_or(expected!(name, bool))
        }

        fn get_f64(&self, name: &str) -> Result<f64, DecoderError> {
            try!(self.get_property(name)).as_f64().ok_or(expected!(name, f64))
        }

        fn get_u64(&self, name: &str) -> Result<u64, DecoderError> {
            try!(self.get_property(name)).as_u64().ok_or(expected!(name, u64))
        }

        // Json -> (Json::String == &str)
        fn get_str(&self, name: &str) -> Result<&str, DecoderError> {
            try!(self.get_property(name)).as_string().ok_or(expected!(name, str))
        }

        // Json -> (Json::String == &str) -> String
        fn get_string(&self, name: &str) -> Result<String, DecoderError> {
            try!(self.get_property(name)).as_owned_string().ok_or(expected!(name, String))
        }
    }

    #[cfg(test)]
    #[allow(non_snake_case)]
    mod test {
        mod JsonExt {
            use std::collections::BTreeMap;

            use super::super::{Json,JsonExt};

            #[test]
            fn into_object() {
                assert!(Json::Object(BTreeMap::new()).into_object().is_ok());
                assert!(Json::Null.into_object().is_err());
            }

            #[test]
            fn as_owned_string() {
                let s: String = "test".into();
                assert!(Json::String(s.clone()).as_owned_string().unwrap() == s);
            }
        }

        mod ObjectExt {
            use std::collections::BTreeMap;

            use super::super::{Json,Object,ObjectExt};

            fn object() -> Object {
                BTreeMap::new()
            }

            #[test]
            fn get_property() {
                assert!(object().get_property("missing").is_err());
            }

            #[test]
            fn get_string_array() {
                let mut object: Object = BTreeMap::new();

                object.insert("strings".to_string(), Json::Array(vec![
                    Json::String("hello world".to_string())
                ]));

                for string in object.get_string_array("strings").unwrap() {
                    assert!(string == "hello world");
                }
            }

            #[test]
            fn get_object_array() {
                let mut object: Object = BTreeMap::new();

                object.insert("objects".to_string(), Json::Array(vec![
                    Json::Object(BTreeMap::new())
                ]));

                assert!(object.get_object_array("objects").is_ok());
            }

            #[test]
            fn get_empty_object() {
                let object: Object = BTreeMap::new();
                assert!(object.get_u64("number").is_err());
            }

            #[test]
            fn get_incorrect_type() {
                let mut object: Object = BTreeMap::new();
                object.insert("nan".to_string(), Json::String("NaN".to_string()));
                assert!(object.get_u64("nan").is_err());
            }
        }
    }
}
