//! Loader functions for actions

use ::rules::actions::Action;
use ::rules::actions::{Nothing,Restart,Command,Signal,Start,Stop};

use super::{LoadObject,ResourceMapExt};
use super::super::{Error,Result};
use super::super::resources::ResourceMap;
use super::super::rustc_serialize::json::{Object,ObjectExt};

impl LoadObject for Action {
    fn load(object: &Object, resources: &ResourceMap) -> Result<Box<Self>> {
        Ok(match try!(object.get_str("type")) {
            "command" => try!(Command::load(object, resources)),
            "kill"    => try!(Signal::load(object, resources)),
            "nothing" => Box::new(Nothing),
            "start"   => try!(Start::load(object, resources)),
            "stop"    => try!(Stop::load(object, resources)),
            "restart" => try!(Restart::load(object, resources)),
            t => return Err(Error::Config(format!("Invalid action type '{}'", t)))
        })
    }
}

impl LoadObject for Command {
    fn load(object: &Object, _: &ResourceMap) -> Result<Box<Self>> {
        Ok(Box::new(Command::new(try!(object.get_string_array("args")))))
    }
}

impl LoadObject for Signal {
    fn load(object: &Object, resources: &ResourceMap) -> Result<Box<Self>> {
        Ok(Box::new(Signal {
            resource: try!(resources.get_process_from_object(object))
        }))
    }
}

impl LoadObject for Start {
    fn load(object: &Object, resources: &ResourceMap) -> Result<Box<Self>> {
        Ok(Box::new(Start {
            resource: try!(resources.get_service_from_object(object))
        }))
    }
}

impl LoadObject for Stop {
    fn load(object: &Object, resources: &ResourceMap) -> Result<Box<Self>> {
        Ok(Box::new(Stop {
            resource: try!(resources.get_service_from_object(object))
        }))
    }
}

impl LoadObject for Restart {
    fn load(object: &Object, resources: &ResourceMap) -> Result<Box<Self>> {
        Ok(Box::new(Restart {
            resource: try!(resources.get_service_from_object(object))
        }))
    }
}

