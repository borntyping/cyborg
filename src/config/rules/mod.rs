//! Loader functions for rules

use ::resources::{Process,Service};
use ::rules::Rule;
use ::rules::actions::Action;
use ::rules::conditions::Condition;
use ::utils::Ref;

use super::{Load,Result};
use super::resources::ResourceMap;
use super::rustc_serialize::json::{JsonExt,Object,ObjectExt};

mod actions;
mod conditions;

/// Type alias for a list of rules.
///
/// This makes up the entire public interface for this module.
pub type Rules = Vec<Rule>;

/// Load a set of rules
impl<'a> Load<(&'a Object, &'a ResourceMap)> for Rules  {
    fn load((object, resources): (&'a Object, &'a ResourceMap)) -> Result<Self> {
        let mut rules = Vec::new();
        for (name, json) in object {
            trace!("Loading rule {:?}", name);
            let object = try!(json.into_object());
            let rule = try!(Rule::load((name.clone(), object, resources)));
            rules.push(rule);
        }
        Ok(rules)
    }
}

/// Load a single rule
impl<'a> Load<(String, &'a Object, &'a ResourceMap)> for Rule {
    fn load((name, object, resources): (String, &'a Object, &'a ResourceMap)) -> Result<Self> {
        let condition_object = try!(object.get_object("when"));
        let condition = try!(Condition::load(condition_object, resources));

        let action_object = try!(object.get_object("then"));
        let action = try!(Action::load(action_object, resources));

        Ok(Rule { name: name, condition: condition, action: action })
    }
}

/// Load a boxed value from a JSON object and the resource map.
///
/// Used to load `Action` and `Condition` values.
trait LoadObject {
    fn load(&Object, &ResourceMap) -> Result<Box<Self>>;
}

/// Added helper methods to `ResourceMap`.
trait ResourceMapExt {
    /// Get a `Process` from a map using the 'resource' property of an object.
    fn get_process_from_object(&self, object: &Object) -> Result<Ref<Process>>;

    /// Get a `Service` from a map using the 'resource' property of an object.
    fn get_service_from_object(&self, object: &Object) -> Result<Ref<Service>>;
}

impl ResourceMapExt for ResourceMap {
    fn get_process_from_object(&self, object: &Object) -> Result<Ref<Process>> {
        self.get_process(try!(object.get_string("resource")))
    }

    fn get_service_from_object(&self, object: &Object) -> Result<Ref<Service>> {
        self.get_service(try!(object.get_string("resource")))
    }
}
