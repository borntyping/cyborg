//! Loader functions for conditions

use ::rules::conditions::Condition;
use ::rules::conditions::{Always,All,Any,CPULimit,MemoryLimit,Never,Command,Status};

use super::{LoadObject,ResourceMapExt};
use super::super::{Error,Load,Result};
use super::super::resources::ResourceMap;
use super::super::rustc_serialize::json::{Object,ObjectExt};

impl LoadObject for Condition {
    fn load(object: &Object, resources: &ResourceMap) -> Result<Box<Self>> {
        Ok(match try!(object.get_str("type")) {
            "always"  => Box::new(Always),
            "all"     => try!(All::load(object, resources)),
            "any"     => try!(Any::load(object, resources)),
            "command" => try!(Command::load(object, resources)),
            "cpu"     => try!(CPULimit::load(object, resources)),
            "memory"  => try!(MemoryLimit::load(object, resources)),
            "never"   => Box::new(Never),
            "status"  => try!(Status::load(object, resources)),
            t => return Err(Error::Config(format!("Invalid condition type '{}'", t)))
        })
    }
}

/// Used by `LoadObject<All>` and `LoadObject<Any>` implementations
fn load_conditions(object: &Object, resources: &ResourceMap) -> Result<Vec<Box<Condition>>> {
    Ok(try!(try!(object.get_object_array("conditions")).iter().map(|object| {
        Condition::load(object, resources)
    }).collect()))
}

impl LoadObject for All {
    fn load(object: &Object, resources: &ResourceMap) -> Result<Box<Self>> {
        Ok(Box::new(All::new(try!(load_conditions(object, resources)))))
    }
}

impl LoadObject for Any {
    fn load(object: &Object, resources: &ResourceMap) -> Result<Box<Self>> {
        Ok(Box::new(Any::new(try!(load_conditions(object, resources)))))
    }
}

impl LoadObject for Command {
    fn load(object: &Object, _: &ResourceMap) -> Result<Box<Self>> {
        let args = try!(object.get_string_array("args"));

        let expect_success = if object.contains_key("expect_success") {
            try!(object.get_boolean("expect_success"))
        } else {
            false
        };

        Ok(Box::new(Command::new(args, expect_success)))
    }
}

impl LoadObject for CPULimit {
    fn load(object: &Object, resources: &ResourceMap) -> Result<Box<Self>> {
        Ok(Box::new(CPULimit::new(
            try!(resources.get_process_from_object(object)),
            try!(object.get_f64("limit"))
        )))
    }
}

impl LoadObject for MemoryLimit {
    fn load(object: &Object, resources: &ResourceMap) -> Result<Box<Self>> {
        Ok(Box::new(MemoryLimit{
            limit: try!(object.get_u64("limit")),
            resource: try!(resources.get_process_from_object(object)),
        }))
    }
}

impl LoadObject for Status {
    fn load(object: &Object, resources: &ResourceMap) -> Result<Box<Self>> {
        Ok(Box::new(Status {
            resource: try!(resources.get_service_from_object(object))
        }))
    }
}

