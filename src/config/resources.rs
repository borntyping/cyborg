//! Load resources from JSON objects

use std::collections::HashMap;

use ::resources::{Resource,Process,Service};
use ::resources::{Cyborg,Pidfile,SysV,Supervisorctl};
use ::utils::Ref;

use super::{Error,Load,Result};
use super::rustc_serialize::json::{JsonExt,Object,ObjectExt};

impl<'l> Load<&'l Object> for Pidfile {
    fn load(object: &Object) -> Result<Self> {
        Ok(Pidfile::new(try!(object.get_string("path"))))
    }
}

impl<'l> Load<&'l Object> for SysV {
    fn load(object: &Object) -> Result<Self> {
        Ok(SysV { name: try!(object.get_string("name")) })
    }
}

impl<'l> Load<&'l Object> for Supervisorctl {
    fn load(object: &Object) -> Result<Self> {
        Ok(Supervisorctl { name: try!(object.get_string("name")) })
    }
}

impl<'l> Load<&'l Object> for Resource {
    fn load(object: &Object) -> Result<Self> {
        Ok(match try!(object.get_str("type")) {
            "cyborg" => Cyborg.into(),
            "pidfile" => try!(Pidfile::load(object)).into(),
            "service" => try!(SysV::load(object)).into(),
            "supervisor" => try!(Supervisorctl::load(object)).into(),

            t => return Err(Error::Config(
                format!("Invalid resource type '{}'", t)))
        })
    }
}

/// Used when configuring rules to fetch resources by name and type
pub struct ResourceMap {
    map: HashMap<String, Resource>
}

impl ResourceMap {
    fn new() -> Self {
        ResourceMap { map: HashMap::new() }
    }

    /// Add a resource to the map
    pub fn set<T: Into<Resource>>(&mut self, name: String, resource: T) {
        self.map.insert(name, resource.into());
    }

    /// Get a resource from the map
    fn get(&self, name: &String) -> Result<&Resource> {
        match self.map.get(name) {
            Some(p) => Ok(p.clone()),
            None => Err(Error::Config(
                format!("No process resource named {}", name)))
        }
    }

    /// Get a Process resource from the map
    pub fn get_process(&self, name: String) -> Result<Ref<Process>> {
        match try!(self.get(&name)) {
            &Resource::Process(ref p) => Ok(p.clone()),
            _ => Err(Error::Config(format!(
                "Resource {} exists but is not a process", name)))

        }
    }

    /// Get a Service resource from the map
    pub fn get_service(&self, name: String) -> Result<Ref<Service>> {
        match try!(self.get(&name)) {
            &Resource::Service(ref p) => Ok(p.clone()),
            _ => Err(Error::Config(format!(
                "Resource {} exists but is not a service", name)))

        }
    }
}

impl<'l> Load<&'l Object> for ResourceMap {
    fn load(object: &Object) -> Result<Self> {
        let mut resources = ResourceMap::new();

        for (resource_name, resource_json) in object.iter() {
            trace!("Loading resource {:?}", resource_name);
            let object = try!(resource_json.into_object());
            resources.set(resource_name.clone(), try!(Resource::load(object)));
        }

        Ok(resources)
    }
}

impl Into<HashMap<String, Resource>> for ResourceMap {
    fn into(self) -> HashMap<String, Resource> {
        self.map
    }
}
