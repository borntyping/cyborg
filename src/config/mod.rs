//! Load Cyborg instances from configuration files written in JSON.
//!
//! This module should be entirely self contained, and references to it should
//! only appear in the main and test executables.

use std::fs::File;
use std::path::Path;

use ::{Cyborg,CyborgArmy};

use self::rustc_serialize::json::{Json,JsonExt,ObjectExt};

pub use self::error::{Error,Result};

mod rustc_serialize;
mod resources;
mod rules;
mod loggers;
mod error;

/// Trait for loading.
///
/// Works like `From` but returns a `Result` instead. This is used to avoid
/// implementing `load` functions outside the `config` module, which in theory
/// can be removed entirely without effecting the rest of the codebase.
///
/// ```
/// # use std::path::Path;
/// use ::cyborg_core::Cyborg;
/// use ::cyborg_core::config::Load;
///
/// Cyborg::load("tests/example.json");
/// Cyborg::load(Path::new("tests/example.json"));
/// ```
pub trait Load<T> {
    fn load(T) -> Result<Self>;
}

/// Load a Cyborg army, converting a lsit of `Strings` to a list of `Path`s
impl Load<Vec<String>> for CyborgArmy {
    fn load(paths: Vec<String>) -> Result<Self> {
        Ok(CyborgArmy { cyborgs: try!(paths.iter().map(|p| {
            Cyborg::load(Path::new(p))
        }).collect())})
    }
}

/// Load a Cyborg instance, converting a `&str` to a `Path`
impl<'l> Load<&'l str> for Cyborg {
    fn load(path: &'l str) -> Result<Self> {
        load(Path::new(path))
    }
}

/// Load a single `Cyborg` instance from a `Path`.
impl<'l> Load<&'l Path> for Cyborg {
    fn load(path: &'l Path) -> Result<Self> {
        info!("Loading configuration from {:?}", path.display());
        let result = load(path);

        match result {
            Ok(_) => info!("Loaded configuration from {:?}", path.display()),
            Err(ref error) => error!("{}", error)
        }

        if let Ok(ref cyborg) = result {
            if cyborg.rules.is_empty() {
                warn!("Config file {} contained no rules", path.display());
            }
        }

        return result;
    }
}

/// Private implementaion used by `Load<&Path> for Cyborg`
fn load(path: &Path) -> Result<Cyborg> {
    let json = try!(Json::from_reader(&mut try!(File::open(path))));
    let object = try!(json.into_object());

    let object_resources = try!(object.get_object("resources"));
    let object_rules     = try!(object.get_object("rules"));
    let object_loggers   = try!(object.get_object("loggers"));

    let resources = try!(self::resources::ResourceMap::load(object_resources));
    let rules     = try!(self::rules::Rules::load((object_rules, &resources)));
    let loggers   = try!(self::loggers::Loggers::load(object_loggers));

    Ok(Cyborg {
        resources: resources.into(),
        rules: rules,
        loggers: loggers
    })
}
