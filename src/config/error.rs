//! `Error` and `Result` types for the `config` module

use std::error::Error as StdError;
use std::io::Error as IoError;

use super::rustc_serialize::json::{DecoderError,ParserError};

/// Result type returned by loader functions.
pub type Result<T> = ::std::result::Result<T, Error>;

/// Errors that might be raised when loading configuration.
#[derive(Debug)]
pub enum Error {
    Io(IoError),            // Errors caused by reading the file
    Decoder(DecoderError),  // Errors caused by parsing the file as JSON
    Config(String)          // Errors caused by invalid configuration
}

impl ::std::fmt::Display for Error {
    fn fmt(&self, f: &mut ::std::fmt::Formatter) -> ::std::fmt::Result {
        match self.cause() {
            Some(cause) => write!(f, "{}: {}", self.description(), cause),
            None => write!(f, "{}", self.description())
        }
    }
}

impl StdError for Error {
    fn description<'a>(&self) -> &'a str {
        "Failed to load configuration"
    }

    fn cause(&self) -> Option<&StdError> {
        match self {
            &Error::Io(ref e) => Some(e),
            _ => None
        }
    }
}

impl From<DecoderError> for Error {
    fn from(err: DecoderError) -> Self {
        match err {
            DecoderError::ApplicationError(e) => Error::Config(e),
            err                               => Error::Decoder(err)
        }
    }
}

impl From<ParserError> for Error {
    fn from(err: ParserError) -> Self {
        Error::Decoder(DecoderError::ParseError(err))
    }
}

impl From<IoError> for Error {
    fn from(err: IoError) -> Self {
        Error::Io(err)
    }
}
