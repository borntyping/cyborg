//! Loader functions for loggers

use ::loggers::RiemannLogger;
use ::loggers::Logger;

use super::{Error,Load,Result};
use super::rustc_serialize::json::{JsonExt,Object,ObjectExt};

pub type Loggers = Vec<Box<Logger>>;

impl<'a> Load<&'a Object> for Loggers {
    fn load(object: &Object) -> Result<Loggers> {
        let mut loggers = Vec::new();
        for (name, json) in object {
            trace!("Loading logger {:?}", name);
            let object = try!(json.into_object());
            loggers.push(try!(Box::load((name.clone(), object))));
        }
        Ok(loggers)
    }
}

impl<'a> Load<(String, &'a Object)> for Box<Logger> {
    /// TODO: Use name as the type when no type is provided
    fn load((name, object): (String, &Object)) -> Result<Box<Logger>> {
        Ok(match try!(object.get_str("type")) {
            "riemann" => {
                let host = try!(object.get_string("host"));
                let port = try!(object.get_u64("port")) as u16;
                Box::new(RiemannLogger::new(name, host, port))
            },
            t => return Err(Error::Config(format!("Invalid logger type '{}'", t)))
        })
    }
}
