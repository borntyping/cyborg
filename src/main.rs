//! Cyborg is a service that runs on a local system and uses a set of user-
//! defined rules to identify and react to problems.
//!
//! It is being written as part of a final year university project. Cyborg is
//! licenced under the [MIT Licence] and written by [Sam Clements]. See the
//! [project homepage][cyborg] for more information.
//!
//! ## Usage
//!
//! Configuration files are written in [JSON] which describe
//! rules and resources. A resource is a JSON object describing something that
//! should be monitored, and a rule describes an action and the conditions it
//! should trigger under.
//!
//! ```json
//! {
//!     "resources": {
//!         "mutiny": {
//!             "type": "pidfile",
//!             "path": "/var/run/mutiny.pid"
//!         }
//!     },
//!     "rules": {
//!         "mutiny": {
//!             "when": {
//!                 "type": "memory",
//!                 "limit": 50000,
//!                 "resource": "mutiny"
//!             },
//!             "then": {
//!                 "type": "kill",
//!                 "resource": "mutiny"
//!             }
//!         }
//!     }
//! }
//! ```
//!
//! Cyborg is intended to run under an init system such as [Supervisord], and so
//! logs directly to `STDOUT` and `STDERR`.
//!
//! [cyborg]: https://github.com/borntyping/cyborg
//! [JSON]: http://json.org/
//! [MIT Licence]: http://opensource.org/licenses/MIT
//! [Sam Clements]: mailto://sam@borntyping.co.uk
//! [Supervisord]: http://supervisord.org/

#![feature(plugin)]
#![plugin(docopt_macros)]

extern crate cyborg_core;
extern crate docopt;
#[macro_use] extern crate log;
extern crate rustc_serialize;
extern crate simple_logger;

use cyborg_core::CyborgArmy;
use cyborg_core::config::Load;

docopt!(Args derive Debug, "
Usage:  cyborg [options] [-c <file>]...
        cyborg (--help | --version)

Options:
    -c <file>, --config=<file>  Configuration file [default: tests/example.json].
    -n, --dry-run               Read configuration but don't start.
    -N, --dry-run-once          Read configuration and tick once.
    -h, --help                  Show this message.
    -v, --version               Show the program version.
");

fn main() {
    simple_logger::init().unwrap();

    let args: Args = Args::docopt().decode().unwrap_or_else(|e| e.exit());
    if args.flag_version {
        println!("cyborg v{}", env!("CARGO_PKG_VERSION"));
        return;
    }

    info!("Starting Cyborg, version {}", env!("CARGO_PKG_VERSION"));
    let mut army = CyborgArmy::load(args.flag_config).unwrap();
    if args.flag_dry_run {
        warn!("Running in dry run mode, stopping");
    } else if args.flag_dry_run_once {
        warn!("Running in dry run mode, will tick once and stop");
        army.once();
    } else {
        army.start();
    }
}
