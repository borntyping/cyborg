//! Loggers collect information about a running Cyborg instance.

use std::error::Error;
use std::fmt::Debug;

use riemann_client::Client;
use riemann_client::proto::Event;
use riemann_client::utils::hostname;

use ::rules::Rule;
use ::rules::Result;

pub trait Logger : Debug {
    fn init(&mut self);

    /// Log information about a rule being checked
    fn log_rule(&mut self, rule: &Rule, rule_result: &Result<bool>);
}

#[derive(Debug)]
pub struct RiemannLogger {
    name: String,
    host: String,
    port: u16,
    client: Option<Client>
}

impl RiemannLogger {
    pub fn new(name: String, host: String, port: u16) -> Self {
        RiemannLogger { name: name, host: host, port: port, client: None }
    }
}

impl Logger for RiemannLogger {
    fn init(&mut self) {
        self.client = Client::connect(&(&self.host[..], self.port)).ok();
    }

    fn log_rule(&mut self, rule: &Rule, rule_result: &Result<bool>) {
        if let Some(ref mut client) = self.client {
            let mut event = Event::new();
            event.set_service(format!("cyborg:rules:{}", rule.name));
            event.set_host(hostname().unwrap());
            event.set_state(match *rule_result {
                Err(_) => "failure",
                Ok(false) => "warning",
                Ok(true) => "okay"
            }.to_string());
            event.set_description(match rule_result {
                &Err(ref e) => e.description(),
                &Ok(false) => "Rule did not match, action was not run",
                &Ok(true) => "Rule matched, action was run"
            }.to_string());

            match client.send_event(event) {
                Ok(msg) => trace!("Sent message to '{}': {:?}", self.name, msg),
                Err(e) => error!("Failed to send message to '{}': {:?}", self.name, e)
            }
        } else {
            warn!("Not connected to configured Riemann server '{}'", self.name);
        }
    }
}
